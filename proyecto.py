#!/usr/bin/env python
#! -- coding: utf-8 --

# importacion de librerias necesarias
import random
import time
import os

# funcion que imprime las matrices
def imprimir(list) :

    for i in range(b):
        for j in range(b):

            print(mat_cero[i][j],end=' ')
        print()

# condidionales para agregar un valor al contador
def contador(mat_cero, x, y, s) :

    cont_x = 0

    if mat_cero[x][y] == s :
        if (x-1) >= 0 and (y-1) >=0:
            if mat_cero[x-1][y-1] == 'X':
                cont_x = cont_x +1
        if (x-1) >= 0:
            if mat_cero[x-1][y] == 'X':
                cont_x = cont_x +1
        if (x-1) >= 0 and (y+1) < b:
            if mat_cero[x-1][y+1] == 'X':
                cont_x = cont_x +1
        if (y-1) >= 0:
            if mat_cero[x][y-1] == 'X':
                cont_x = cont_x +1
        if (y+1) < b:
            if mat_cero[x][y+1] == 'X':
                cont_x = cont_x +1
        if (x+1) < b and (y-1) >= 0:
            if mat_cero[x+1][y-1] == 'X':
                cont_x = cont_x +1
        if (x+1) < b:
            if mat_cero[x+1][y] == 'X':
                cont_x = cont_x +1
        if (x+1) < b and (y+1) < b:
            if mat_cero[x+1][y+1] == 'X':
                cont_x = cont_x +1

        return cont_x

# condiciones para saber so sobreviven
def condicion_vivas(valor) :
    if valor >= 4 or valor < 2:
        respuesta = '.'
    if valor == 3 or valor == 2 :
        respuesta = 'X'

    return respuesta

# condiciones para saber si reviven
def condicion_muertas(valor):
    if valor > 3 or valor < 3 :
        respuesta = '.'
    if valor == 3 :
        respuesta = 'X'

    return respuesta

# funcion que compara ambas matrices para saber el estado actual
def estado_celular(temp_list, mat_cero) :

    vivas = 0
    sobreviven = 0
    murioo = 0
    muertas = 0
    sigue = 0
    revive = 0
    for i in range(b):
        for j in range(b):
            if mat_cero[i][j] == 'X' :
                vivas = vivas +1
            if temp_list[i][j] == 'X' :
                if temp_list[i][j] == mat_cero[i][j]:
                    sobreviven = sobreviven +1
                if mat_cero[i][j] != temp_list[i][j] :
                    murioo = murioo +1
            if mat_cero[i][j] == '.' :
                muertas = muertas +1
            if temp_list[i][j] == '.' :
                if temp_list[i][j] == mat_cero[i][j]:
                    sigue = sigue +1
                if temp_list[i][j] != mat_cero[i][j]:
                    revive = revive +1
    # 00declaracion en pantalla de las cuentas
    print('Vivas actuales ', vivas, 'Muertas actuales ', muertas)
    print('Se murieron ', murioo, 'Sobrevivieron ', sobreviven)
    print('Siguen muertas ', sigue, 'Revivieron ', revive)


mat_cero = []
temp_list = []
b = 25
# seccion dedicada a ingresar listas dentro de una lista y rellenar espacios
for i in range(b):
    # agrega listas que actuan como filas
    mat_cero.append([])
    temp_list.append([])

    for j in range(b):
        # random para dar aleatoriedad a la matriz
        r = random.randint(1, 100)
        if r < 80 :
            r = '.'
        elif r >= 80 :
            r = 'X'
        # inserta en la lista el "estado de la celula"
        mat_cero[i].append(str(r))
        temp_list[i].append(str(r))

z = random.randint(5, 50)
# ciclo para repetir el sistema y deribbar nuevas generaciones
for l in range(z):

    # la generacion 0 no se puede comparar con nada asi que inicia en 0
    if l == 0 :
        vivas = 0
        sobreviven = 0
        murio = 0
        muertas = 0
        sigue = 0
        revive = 0
        print('Vivas actuales ', vivas, 'Muertas actuales ', muertas)
        print('Se murieron ', murio, 'Sobrevivieron ', sobreviven)
        print('Siguen muertas ', sigue, 'Revivieron ', revive)
        imprimir(mat_cero)
        print('GENERACION: ', l)
    # actualiza la matriz temporal
    if l >= 1 :
        for x in range(b):
            for y in range(b):
                temp_list[x][y] = mat_cero[x][y]
    # contador celulas vecinas caso vivas
    for x in range(b):
        for y in range(b):

            if temp_list[x][y] == 'X' :
                s = 'X'
                cont_x = contador(temp_list, x, y, s)
                respuesta_x = condicion_vivas(cont_x)
                mat_cero[x][y] = respuesta_x
    # contador celulas vecinas caso muertas
    for x in range(b):
        for y in range(b):

            if temp_list[x][y] == '.' :
                m = '.'
                conto = contador(temp_list, x, y, m)
                respuestao = condicion_muertas(conto)
                mat_cero[x][y] = respuestao
    # detener el tiempo para analizar pantall
    time.sleep(1)
    # limpiar pantalla
    os.system("clear")
    # comparaciones entre matrices
    estado_celular(temp_list, mat_cero)
    imprimir(mat_cero)
    print('GENERACION: ', l+1)
