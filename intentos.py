#!/usr/bin/env python
#! -*- coding: utf-8 -*-

# importar una "libreria" necesaria para ejercutare el random
import random
import time
import os

# funcion que imprime las matrices
def imprimir(list) :
    for i in range(b):
        for j in range(b):
            print(a[i][j],end=' ')
        print()

def contador(a, x, y, s) :
    # reinicio del contador
    contx = 0
    # condidionales para agregar un valor al contador
    if a[x][y] == s :
        if (x-1) >= 0 and (y-1) >=0:
            if a[x-1][y-1] == 'X':
                contx = contx +1
        if (x-1) >= 0:
            if a[x-1][y] == 'X':
                contx = contx +1
        if (x-1) >= 0 and (y+1) < b:
            if a[x-1][y+1] == 'X':
                contx = contx +1
        if (y-1) >= 0:
            if a[x][y-1] == 'X':
                contx = contx +1
        if (y+1) < b:
            if a[x][y+1] == 'X':
                contx = contx +1
        if (x+1) < b and (y-1) >= 0:
            if a[x+1][y-1] == 'X':
                contx = contx +1
        if (x+1) < b:
            if a[x+1][y] == 'X':
                contx = contx +1
        if (x+1) < b and (y+1) < b:
            if a[x+1][y+1] == 'X':
                contx = contx +1
        return contx

def condicion_vivas(valor) :
    if valor >= 4 or valor < 2:
        respuesta = '.'
    if valor == 3 or valor == 2 :
        respuesta = 'X'

    return respuesta

def condicion_muertas(valor):
    if valor > 3 or valor < 3 :
        respuesta = '.'
    if valor == 3 :
        respuesta = 'X'

    return respuesta

def estado_celular(temp_list, a) :

    vivas = 0
    sobreviven = 0
    murio = 0
    muertas = 0
    sigue = 0
    revive = 0
    for i in range(b):
        for j in range(b):
            if a[i][j] == 'X' :
                vivas = vivas +1
            if temp_list[i][j] == 'X' :
                if temp_list[i][j] == a[i][j]:
                    sobreviven = sobreviven +1
                if temp_list[i][j] != a[i][j]:
                    murio = murio +1
            if a[i][j] == '.' :
                muertas = muertas +1
            if temp_list[i][j] == '.' :
                if temp_list[i][j] == a[i][j]:
                    sigue = sigue +1
                if temp_list[i][j] != a[i][j]:
                    revive = revive +1

    print('vivas actuales ', vivas, 'muertas actuales ', muertas)
    print('se mamaron ', murio, 'se mantuvieron vivas ', sobreviven)
    print('siguen muertas ', sigue, 'revivieron como yisus ', revive)

a = []
temp_list = []
b = 20

# seccion dedicada a ingresar listas dentro de una lista y rellenar espacios
for i in range(b):
    a.append([])
    temp_list.append([])
    for j in range(b):
        # se obtienen valores aleatoreos para celulas vivas o muertas
        r = random.randint(1, 100)
        if r < 80 :
            r = '.'
        elif r >= 80 :
            r = 'X'
        # inserta en la lista el "estado de la celula"
        a[i].append(str(r))
        temp_list[i].append(str(r))

z = random.randint(5, 20)

for l in range(z):

    if l == 0 :
        vivas = 0
        sobreviven = 0
        murio = 0
        muertas = 0
        sigue = 0
        revive = 0
        print('vivas actuales ', vivas, 'muertas actuales ', muertas)
        print('se mamaron ', murio, 'se mantuvieron vivas ', sobreviven)
        print('siguen muertas ', sigue, 'revivieron como yisus ', revive)
        imprimir(a)
        print('GENERACION: ', l)

    if l >= 1 :
        temp_list = a[:]

    for x in range(b):
        for y in range(b):

            if temp_list[x][y] == 'X' :
                s = 'X'
                contx = contador(temp_list, x, y, s)
                respuestax = condicion_vivas(contx)
                a[x][y] = respuestax

    for x in range(b):
        for y in range(b):
            if temp_list[x][y] == '.' :
                m = '.'
                conto = contador(temp_list, x, y, m)
                respuestao = condicion_muertas(conto)
                a[x][y] = respuestao

    time.sleep(1.5)
    os.system("clear")

    estado_celular(temp_list, a)
    imprimir(a)
    print('GENERACION: ', l+1)
